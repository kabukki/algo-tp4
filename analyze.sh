#!/bin/bash

OUT=$(mktemp)

if [[ $# -eq 2 ]]; then
	DICTIONARY=$1
	SIZE=$2

	if [[ -f $1 ]]; then
		# Crée le fichier
		./hash $SIZE <<- END
			plot $DICTIONARY $OUT
			analyser $DICTIONARY
		END

		# Affiche l'histogramme
		gnuplot <<- END
			set terminal dumb # size 168, 24
			set xrange [0:]
			set yrange [0:]
			set autoscale
			set style fill solid
			plot "$OUT" with boxes
		END

		# Supprime le fichier contenant les données
		rm $OUT
	else
		echo "Fichier $1 introuvable"
	fi
else
	echo "Syntaxe: $0 <fichier> <taille de la table>"
fi
