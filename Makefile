BIN	=	hash

SRC	=	src/main.cpp		\
		src/bit-helpers.cpp	\
		src/Color.cpp		\
		src/Table.cpp		\
		src/Hash.cpp		\
		src/CLI.cpp
OBJ	=	$(SRC:.cpp=.o)

INC	=	-I include
LIB	=

CXXFLAGS	=	-g -W -Wall -Wextra
CC			=	g++ $(CXXFLAGS)
RM			=	rm -rf

# Linking
$(BIN):	$(OBJ)
	$(CC) $^ $(INC) $(LIB) -o $@

# Compilation
%.o:	%.cpp
	$(CC) -c $< $(INC) -o $@

# Extra rules
all:	$(BIN)

clean:
	$(RM) $(OBJ)
fclean:	clean
	$(RM) $(BIN)
re: fclean all

.PHONY:	$(BIN)
