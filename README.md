# TP 4

TP 4 du cours de structures de données & algorithmes.

## Compilation & exécution

```bash
$ make
$ ./hash <taille de la table>
```

## Scripts

- `analyze.sh`: produit un fichier contenant les données d'un histogramme de test. Puis affiche les statistiques, et affiche le graphe grâce à **gnuplot**.
- `fill.sh`: lance le programme et ajoute tous les mots présents dans un fichier donné.
