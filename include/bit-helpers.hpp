#ifndef __MODULE_BIT_HELPERS__
#define __MODULE_BIT_HELPERS__

#include <cstdint>
#include <cstddef>

namespace bit_helpers {

 // The compiler should optimize them away
 // using ror and rol.
 //
 inline uint64_t ror(uint64_t x, int r) { return (x>>r) | (x<<(64-r)); }
 inline uint64_t rol(uint64_t x, int r) { return (x<<r) | (x>>(64-r)); }
 inline uint64_t to_gray(uint64_t x) { return x^(x>>1); }
 inline uint64_t cut_deck(uint64_t x) { return (x<<32) | (x>>32); }
 
 uint64_t reverse(uint64_t x);

 uint64_t blender (uint64_t x);
 uint64_t blender2(uint64_t x);
 uint64_t blender6(uint64_t x);
 uint64_t perfect_shuffle(uint64_t x);
 
} // namespace

#endif
    // __MODULE_BIT_HELPERS__
