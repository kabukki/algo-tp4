#ifndef TABLE_HPP
#define TABLE_HPP

#include <iostream>
#include <vector>
#include <list>
#include "Hash.hpp"

class Table
{
private:
	std::vector<std::list<std::string>>		_table;
	size_t									_nwords;

public:
	Table(const size_t size);
	~Table();

	static hash_uint_t	hash(const char buffer[], size_t size);

	bool				add(const std::string & word);
	bool				remove(const std::string & word);
	std::string *		search(const std::string & word);

	size_t				size() const;
	size_t				nwords() const;
	std::list<std::string>	operator[](size_t n) const;
};

#endif