#ifndef CLI_HPP
#define CLI_HPP

#include <vector>
#include <string>
#include "Table.hpp"

class CLI
{
private:
	Table		_table;

	bool		readInput(std::vector<std::string> & args, const std::string & prompt);
	bool		readInput(std::vector<std::string> & args);
	void		greet();
	void		goodbye();

	void		add(const std::vector<std::string> & args);
	void		remove(const std::vector<std::string> & args);
	void		search(const std::vector<std::string> & args);
	void		print(const std::vector<std::string> & args);
	void		analyze(const std::vector<std::string> & args);
	void		plot(const std::vector<std::string> & args);
	void		help(const std::vector<std::string> & args);

public:
	CLI(const size_t size);
	~CLI();

	void		start();
};

#endif
