#ifndef HASH_HPP
#define HASH_HPP

#include <cstddef>
#include <cstdint>

using hash_uint_t = uint64_t;

class Hash
{
public:
	static hash_uint_t	checksum(const char buffer[], size_t size);
	static hash_uint_t	knuth(const char buffer[], size_t size);
	static hash_uint_t	lcg(const char buffer[], size_t size);
	static hash_uint_t	bricolage(const char buffer[], size_t size);
	static hash_uint_t	oneAtATime(const char buffer[], size_t size);
	static hash_uint_t	lucien(const char buffer[], size_t size);
};

#endif
