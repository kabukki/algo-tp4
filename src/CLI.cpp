#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <map>
#include "CLI.hpp"
#include "Color.hpp"

/**
 * Typedef pour les pointeurs sur fonction
 */
typedef void (CLI::*func)(const std::vector<std::string> &);

CLI::CLI(const size_t size) : _table(Table(size)) {}

CLI::~CLI() {}

/**
 * Lit en entrée et remplit les arguments 'tokenisés', en affichant un prompt
 */
bool	CLI::readInput(std::vector<std::string> & args, const std::string & prompt)
{
	std::string line;

	std::cout << Color::BOLD << prompt << "> " << Color::RESET;

	if (std::getline(std::cin, line)) {
		std::string			arg;
		std::istringstream	iss(line);

		while (std::getline(iss, arg, ' ')) {
			if (!arg.empty()) {
				args.push_back(arg);
			}
		}

		return true;
	} else {
		return false;
	}
}

/**
 * Lit en entrée et remplit les arguments 'tokenisés'
 */
bool	CLI::readInput(std::vector<std::string> & args)
{
	return readInput(args, "");
}

/**
 * Affiche un message de bienvenue
 */
void	CLI::greet()
{
	std::cout << Color::FG_CYAN << "Bienvenue dans le tp 3 !" << Color::RESET << std::endl;
	std::cout << "Tapez " << Color::BOLD << "aide" << Color::RESET << " pour afficher la liste des commandes disponibles." << std::endl;
}

/**
 * Affiche un message d'au revoir
 */
void	CLI::goodbye()
{
	std::cout << Color::FG_CYAN << "A bientôt !" << Color::RESET << std::endl;
}

/**
 * Lance l'interface en ligne de commandes
 */
void	CLI::start()
{
	std::vector<std::string>	args;
	std::map<std::string, func> commands = {
		{ "ajouter", &CLI::add },
		// Synonymes
		{ "retirer", &CLI::remove },
		{ "supprimer", &CLI::remove },
		{ "enlever", &CLI::remove },
		// Synonymes
		{ "chercher", &CLI::search },
		{ "rechercher", &CLI::search },
		{ "trouver", &CLI::search },
		{ "afficher", &CLI::print },
		{ "analyser", &CLI::analyze },
		{ "plot", &CLI::plot },
		{ "aide", &CLI::help }
	};

	greet();

	while (readInput(args, "hash")) {
		if (args.size() == 0) continue;
		const auto& cmd = args[0];

		try {
			if (cmd == "quitter") {
				break;
			} else {
				auto it = commands.find(cmd);

				if (it != commands.end()) {
					(this->*(it->second))(args);
				} else {
					std::cerr << "Commande inconnue" << std::endl;
				}
			}
		} catch (std::exception& err) {
			std::cerr << err.what() << std::endl;
		}
		args.clear();
	}

	goodbye();
}

/**
 * Ajoute un ou plusieurs mots dans la table
 */
void	CLI::add(const std::vector<std::string> & args)
{
	if (args.size() >= 2) {
		for (size_t n = 1; n < args.size(); ++n) {
			const std::string word = args[n];

			if (_table.add(word)) {
				std::cout << Color::FG_GREEN << "Ajouté " << word << " dans la table." << Color::RESET << std::endl;
			} else {
				std::cerr << Color::FG_YELLOW << word << " est déjà dans la table." << Color::RESET << std::endl;
			}
		}
	} else {
		std::cerr << Color::FG_RED << "Vous devez spécifier au moins un mot à ajouter." << Color::RESET << std::endl;
	}
}

/**
 * Retire un ou plusieurs mots de la table
 */
void	CLI::remove(const std::vector<std::string> & args)
{
	if (args.size() >= 2) {
		for (size_t n = 1; n < args.size(); ++n) {
			const std::string word = args[n];

			if (_table.remove(word)) {
				std::cout << Color::FG_GREEN << "Retiré " << word << " de la table." << Color::RESET << std::endl;
			} else {
				std::cerr << Color::FG_YELLOW << word << " n'est pas dans la table." << Color::RESET << std::endl;
			}
		}
	} else {
		std::cerr << Color::FG_RED << "Vous devez spécifier au moins un mot à retirer." << Color::RESET << std::endl;
	}
}

/**
 * Recherche un ou plusieurs mots dans la table
 */
void	CLI::search(const std::vector<std::string> & args)
{
	if (args.size() >= 2) {
		for (size_t n = 1; n < args.size(); ++n) {
			const std::string word = args[n];
			const auto found = _table.search(word);
	
			if (found != nullptr) {
				std::cout << Color::FG_GREEN << "Trouvé " << *found << " dans la table." << Color::RESET << std::endl;
			} else {
				std::cerr << Color::FG_YELLOW << word << " introuvable dans la table." << Color::RESET << std::endl;
			}
		}
	} else {
		std::cerr << Color::FG_RED << "Vous devez spécifier au moins un mot à chercher." << Color::RESET << std::endl;
	}
}

/**
 * Affiche la table
 */
void	CLI::print(const std::vector<std::string> &)
{
	// Nombre d'espaces pour le padding
	const size_t		nspaces = std::log10(_table.size()) + 1;

	std::cout << "La table contient " << _table.nwords() << " mots." << std::endl;
	for (size_t n = 0; n < _table.size(); ++n) {
		const auto&	list = _table[n];

		std::cout << std::setw(nspaces) << n << ": ";
		if (!list.empty()) {
			std::string comma = "";

			std::cout << Color::FG_CYAN;
			for (const auto& word : list) {
				std::cout << comma << word;
				comma = ", ";
			}
			std::cout << Color::RESET;
		} else {
			std::cout << "<vide>";
		}
		std::cout << std::endl;
	}
}

/**
 * Évalue la fonction de hachage de la table en analysant la distribution de clés depuis un fichier.
 */
void	CLI::analyze(const std::vector<std::string> & args)
{
	if (args.size() == 2) {
		const std::string	path = args[1];
		std::ifstream		file(path);
		const size_t		size = _table.size();
	
		if (file.is_open()) {
			size_t			obs[size] = { 0 };

			// Remplit les valeurs observées pour chaque clé présente dans le fichier
			std::string		word;
			while (std::getline(file, word)) {
				++obs[Table::hash(word.c_str(), word.size()) % size];
			}

			// La moyenne
			double			mean = 0;
			for (size_t n = 0; n < size; ++n) {
				mean += obs[n];
			}
			mean /= size;

			// La variance
			double			variance = 0;
			for (size_t n = 0; n < size; ++n) {
				variance += std::pow(obs[n] - mean, 2);
			}
			variance /= size;

			std::cout << "La moyenne est de: " << mean << std::endl;
			std::cout << "La variance est de: " << variance << std::endl;
			std::cout << "L'écart type est de: " << std::sqrt(variance) << std::endl;

			file.close();
		} else {
			std::cerr << Color::FG_YELLOW << "Impossible d'ouvrir le fichier " << path << Color::RESET << std::endl;
		}
	} else {
		std::cerr << Color::FG_RED << "Vous devez spécifier le fichier contenant les mots à utiliser." << Color::RESET << std::endl;
	}
}

/**
 * Produit un fichier utilisable par gnuplot pour produire un histogramme visuel
 */
void	CLI::plot(const std::vector<std::string> & args)
{
	if (args.size() == 3) {
		const std::string	pathIn = args[1];
		std::ifstream		input(pathIn);
		// Taille de l'histogramme
		const size_t		size = _table.size();
	
		if (input.is_open()) {
			const std::string	pathOut = args[2];
			std::ofstream		output(pathOut);
			size_t				obs[size] = { 0 };

			if (output.is_open()) {
				// Remplit les valeurs observées pour chaque clé présente dans le fichier
				std::string		word;
				while (std::getline(input, word)) {
					++obs[Table::hash(word.c_str(), word.size()) % size];
				}

				// Écrit le contenu
				output << "# Cases Items par case" << std::endl;
				for (size_t n = 0; n < size; ++n) {
					output << n << " " << obs[n] << std::endl;
				}

				output.close();
			} else {
				std::cerr << Color::FG_YELLOW << "Impossible d'écrire dans le fichier " << pathOut << Color::RESET << std::endl;
			}
			input.close();
		} else {
			std::cerr << Color::FG_YELLOW << "Impossible de lire le fichier " << pathIn << Color::RESET << std::endl;
		}
	} else {
		std::cerr << Color::FG_RED << "Vous devez spécifier le fichier contenant les mots à utiliser et le fichier de sortie." << Color::RESET << std::endl;
	}
}

/**
 * Affiche la liste des commandes
 */
void	CLI::help(const std::vector<std::string> &)
{
	std::cout
		<< "Commandes disponibles:" << std::endl
		<< "    ajouter <mots...>                     ajoute un ou plusieurs mots à la table." << std::endl
		<< "                                              mots: liste de mots à ajouter" << std::endl
		<< "    retirer|supprimer|enlever <mots...>   retire un ou plusieurs mots de la table." << std::endl
		<< "                                              mots: liste de mots à enlever" << std::endl
		<< "    chercher|rechercher|trouver <mots...> chercher un ou plusieurs mots dans la table." << std::endl
		<< "                                              mots: liste de mots à retrouver" << std::endl
		<< "    afficher                              affiche le contenu de la table." << std::endl
		<< "    analyser <fichier>                    analyse l'algorithme de hachage (calcule l'écart-type) pour une table de la taille courante" << std::endl
		<< "                                              fichier: fichier contenant la liste de mots utilisés pour évaluer la distribution des clés" << std::endl
		<< "    plot <entrée> <sortie>                produit un fichier consommable par gnuplot contenant l'histogramme de la taille de la table actuelle" << std::endl
		<< "                                              entrée: fichier contenant la liste de mots utilisés pour évaluer la distribution des clés" << std::endl
		<< "                                              sortie: fichier où écrire les données" << std::endl
		<< "    aide                                  affiche cette aide" << std::endl
		<< "    quitter                               quitte le menu" << std::endl;
}
