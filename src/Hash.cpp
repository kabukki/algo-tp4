#include "Hash.hpp"
#include "bit-helpers.hpp"

/**
 * Checksum
 */
hash_uint_t			Hash::checksum(const char buffer[], size_t size)
{
	hash_uint_t				h = 0;

	for (size_t n = 0; n < size; ++n) {
		h += buffer[n];
	}

	return h;
}

/**
 * Knuth
 */
hash_uint_t			Hash::knuth(const char buffer[], size_t size)
{
	hash_uint_t				h = 0;

	for (size_t n = 0; n < size; ++n) {
		h = (h << 5) ^ (h >> 59) ^ buffer[n];
	}

	return h;
}

/**
 * Linear congruential generator
 */
hash_uint_t			Hash::lcg(const char buffer[], size_t size)
{
	const unsigned char *	u = (unsigned char *)buffer;
	hash_uint_t				h = 0;

	for (size_t n = 0; n < size; ++n) {
		h = (h * hash_uint_t { 1103515245 }) + 12345 + u[n];
	}

	return h;
}

/**
 * Bricolage (depuis le livre, p. 286)
 */
hash_uint_t			Hash::bricolage(const char buffer[], size_t size)
{
	const hash_uint_t		magic = { 173773926194192273 };
	hash_uint_t				h = 0;

	for (size_t n = 0; n < size; ++n) {
		h = bit_helpers::cut_deck(h) + (buffer[n] * magic);
	}

	return h;
}

/**
 * Fonction 'one at a time' de Bob Jenkins
 */
hash_uint_t			Hash::oneAtATime(const char buffer[], size_t size)
{
	hash_uint_t				h = 0;

	for (size_t n = 0; n < size; ++n) {
		h += buffer[n];
		h += h << 10;
		h ^= h >> 6;
	}

	h += h << 3;
	h ^= h >> 11;
	h += h << 15;

	return h;
}

/**
 * Ma propre fonction
 */
hash_uint_t			Hash::lucien(const char buffer[], size_t size)
{
	hash_uint_t				h = 0;

	for (size_t n = 0; n < size; ++n) {
		h += buffer[n] * 2;
		h += h << 3;
		h ^= 5;
		h += bit_helpers::to_gray(7) / 11;
		h += 13 * 12345 / h;
	}

	return h + (h << 13);
}
