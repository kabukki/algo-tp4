#include <iostream>
#include "CLI.hpp"

int		main(int ac, char **av)
{
	if (ac == 2) {
		const size_t	size = std::stoi(av[1]);

		if (size > 0) {
			try {
				CLI cli(size);
				cli.start();
			} catch (std::exception& e) {
				std::cerr << "Error: " << e.what() << std::endl;
			}
		} else {
			std::cerr << "La taille de la table doit être strictement positive." << std::endl;
		}
	} else {
		std::cerr << "Syntaxe: " << av[0] << " <taille de la table>" << std::endl;
	}

	return 0;
}
