#include <algorithm>
#include "Table.hpp"
#include "Hash.hpp"

Table::Table (const size_t size)
{
	_table.resize(size);
	_nwords = 0;
}

Table::~Table () {}

/**
 * Génère un hash à partir du buffer donné en entrée
 */
hash_uint_t			Table::hash(const char buffer[], size_t size)
{
	return Hash::lucien(buffer, size);
}

/**
 * Ajoute un mot dans la table si non présent
 */
bool				Table::add(const std::string & word)
{
	if (search(word) == nullptr) {
		const size_t address = hash(word.c_str(), word.size()) % size();
		auto& list = _table[address];

		list.push_front(word);
		++_nwords;
		return true;
	} else {
		return false;
	}
}

/**
 * Retire un mot de la table si présent
 */
bool				Table::remove(const std::string & word)
{
	const size_t address = hash(word.c_str(), word.size()) % size();
	auto& list = _table[address];
	auto it = std::find(list.begin(), list.end(), word);

	if (it != list.end()) {
		list.erase(it);
		--_nwords;
		return true;
	} else {
		return false;
	}
}

/**
 * Retourne un pointeur vers le mot si présent dans la table ou nullptr
 */
std::string *		Table::search(const std::string & word)
{
	const size_t address = hash(word.c_str(), word.size()) % size();
	auto& list = _table[address];
	auto it = std::find(list.begin(), list.end(), word);

	if (it != list.end()) {
		return &(*it);
	} else {
		return nullptr;
	}
}

/**
 * Taille de la table
 */
size_t				Table::size() const
{
	return _table.size();
}

/**
 * Nombre de mots dans la table
 */
size_t				Table::nwords() const
{
	return _nwords;
}

/**
 * Accède à la nième case de la table
 */
std::list<std::string>		Table::operator[](size_t n) const
{
	return _table[n];
}
