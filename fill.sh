#!/bin/bash

WORDS=()

if [[ $# -eq 2 ]]; then
	DICTIONARY=$1
	SIZE=$2

	if [[ -f $1 ]]; then
		# Charge les mots
		while read -r line; do
			WORDS+=($line)
		done < $DICTIONARY

		# Les fournit à la table
		./hash $SIZE <<- END
			ajouter ${WORDS[@]}
			afficher
		END
	else
		echo "Fichier $1 introuvable"
	fi
else
	echo "Syntaxe: $0 <fichier> <taille de la table>"
fi
